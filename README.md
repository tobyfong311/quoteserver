

## DEMO

To demonstrate how to use the QuoteServer, please follow below steps to run the Demo Test:

1. Import as Maven project
2. Run the main method of QuoteServerDemo(in the test classes) to start up the Quote Server
3. Run the main method of TestClientDemo(in the test classes) to spawn 100 testing clients

---

## NOTES

1.  There will be a limit for how many connections the server can handle concurrently (Configurable through THREAD_NUM in the QuoteServer.java)
    New Connection will have to wait for available connections before server can response

2. Mock implementation of QuoteCalculationEngine and ReferencePriceSource is used for demo purpose
    - QuoteCalculationEngineMockImpl will return the reference Price of the security id after a 3s delay to mimic a long calculation time
    - ReferencePriceSourceMockImpl makes all security prices increase by 1 every 5 seconds and notify QuoteServer to update its refPriceMap upon price change
    - The default reference price for every security is always  securityId * 2 for demo purpose

---

## ASSUMPTIONS

1. All the dependencies are injected into QuoteServer. Such as the security list, QuoteCalculationEngine and ReferencePriceSource.

2. The server will be able to consume and process all the queued client's connection since the requests has been load balanced

3. Server will reply ERROR FORMAT if client's request is malformed

4. Server will reply NO_SECURITY_ID if the client asks for a non-existent security

5. The real implementation of quoteCalculationEngine is thread-safe
