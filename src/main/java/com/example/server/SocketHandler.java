package com.example.server;

import com.example.marketmaker.QuoteCalculationEngine;
import com.example.model.QuoteRequest;
import com.example.util.QuoteRequestParser;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ConcurrentMap;

/**
 *  This class is mainly responsible for responding the clients' request with calculated price
 */
public class SocketHandler implements Runnable {
    Socket socket;
    QuoteCalculationEngine quoteCalculationEngine;
    ConcurrentMap<Integer, Double> refPriceMap;

    public SocketHandler (Socket socket, QuoteCalculationEngine quoteCalculationEngine, ConcurrentMap<Integer, Double> refPriceMap){
        this.socket = socket;
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.refPriceMap = refPriceMap;
    }

    public void run() {
        try(InputStream inputStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true)
        ){
            String line = reader.readLine();
            System.out.println("Receive from Client = " + line);

            // Parse the input and construct the request object
            QuoteRequest request = QuoteRequestParser.parseInput(line);
            String message;
            if (request!=null){
                int securityId = request.getSecurityId();
                boolean buy = request.isBuy();
                int quantity = request.getQuantity();

                if (refPriceMap.containsKey(securityId)) {
                    double refPrice = refPriceMap.get(securityId);
                    double quote = quoteCalculationEngine.calculateQuotePrice(securityId, refPrice, buy, quantity);
                    message = Double.toString(quote);
                }
                else {
                    message = "NO_SECURITY_ID";
                }
            }
            else{
                message = "ERROR FORMAT";
            }
            writer.println(message);
            System.out.println("Return to Client = " + message);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
