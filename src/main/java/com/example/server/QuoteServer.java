package com.example.server;

import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePriceSource;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * This class will start TCP server, and delegate response handling to SocketHandler via new thread
 * It also maintains a reference price map that contains all the reference prices for the securities
 */
public class QuoteServer {
    QuoteCalculationEngine quoteCalculationEngine;
    ReferencePriceSource referencePriceSource;
    List<Integer> securityIds;

    // How many clients the server can support concurrently
    int threadNo;

    // <security_id, reference_price>
    ConcurrentMap<Integer, Double> refPriceMap = new ConcurrentHashMap<>();

    public QuoteServer(List<Integer> securityIds, QuoteCalculationEngine quoteCalculationEngine, ReferencePriceSource referencePriceSource, int threadNo){
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceSource = referencePriceSource;
        this.securityIds = securityIds;
        this.threadNo = threadNo;
    }

    private void initRefPriceMap(){
        for (int i=0; i<securityIds.size();i++){
            double refPrice = referencePriceSource.get(securityIds.get(i));
            refPriceMap.put(securityIds.get(i), refPrice);
        }
    }

    private void subscribeForPriceChanged(){
        referencePriceSource.subscribe((securityId, price) -> {
            refPriceMap.put(securityId, price);
            System.out.println("Updated Security " + securityId + " reference price to: "+ price);
        });
    }

    public void start() throws IOException {
        // Init the refPriceMap
        initRefPriceMap();

        // Subscribe for further price changes
        subscribeForPriceChanged();

        // Spawn a executor Service thread pool for handling the clients concurrently
        ExecutorService executorService  = Executors.newFixedThreadPool(threadNo);
        ServerSocket serverSocket = new ServerSocket(8888);
        System.out.println("Waiting for socket connect");
        while (true){
            try {
                Socket socket = serverSocket.accept(); //establishes connection
                executorService.execute(new SocketHandler(socket, quoteCalculationEngine, refPriceMap));
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }

}
