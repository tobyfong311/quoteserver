package com.example.util;

import com.example.model.QuoteRequest;

public class QuoteRequestParser {
    private QuoteRequestParser(){
        throw new IllegalStateException("Utility class");
    }

    /**
     *
     * @param input - The input format will always be  {security ID} (BUY|SELL) {quantity}
     *                             For Example - 123 BUY 100
     *
     * @return The QuoteRequest object, or null if the parse has gone wrong
     *
     */
    public static QuoteRequest parseInput(String input){

        try {
            String[] parts = input.split(" ");
            if (parts.length != 3) {
                return null;
            }
            int securityId = Integer.parseInt(parts[0]);
            boolean buy;
            if ("BUY".equals(parts[1])){
                buy = true;
            }
            else if ("SELL".equals(parts[1])){
                buy = false;
            }
            else {
                return null;
            }
            int quantity = Integer.parseInt(parts[2]);

            return new QuoteRequest(securityId, buy, quantity);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
