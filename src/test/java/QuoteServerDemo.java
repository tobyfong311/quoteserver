import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePriceSource;
import com.example.server.QuoteServer;
import com.example.test.mock.QuoteCalculationEngineMockImpl;
import com.example.test.mock.ReferencePriceSourceMockImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class QuoteServerDemo {
    public static void main(String[] args) {
        // How many clients the server can support concurrently
        final int THREAD_NUM = 50;

        // Only add two security IDs for demonstration
        final List<Integer> securityIds = new ArrayList<>();
        securityIds.add(123);
        securityIds.add(150);

        // Run the Quote Server with Mock Implementation of QuoteCalculationEngine and ReferencePriceSource
        new Thread(() -> {
            QuoteCalculationEngine quoteCalculationEngine = new QuoteCalculationEngineMockImpl();
            ReferencePriceSource referencePriceSource = new ReferencePriceSourceMockImpl(securityIds);
            QuoteServer quoteServer = new QuoteServer(securityIds, quoteCalculationEngine, referencePriceSource, THREAD_NUM);
            try {
                quoteServer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
