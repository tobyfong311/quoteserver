import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TestClientDemo {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public String sendMessage(String msg) throws IOException {
        out.println(msg);
        String resp = in.readLine();
        return resp;
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    public static void main(String[] args) {
        // Spawn 100 clients to connect concurrently
        // The last two messages was deliberately wrong for testing the NO_SECURITY_ID and ERROR FORMAT response
        String[] messages = {"123 BUY 100", "150 SELL 20", "140 BUY 20", "123 BIG 40"};
        for (int i=0;i<100;i++){
            final int count = i;
            new Thread(() -> {
                try {
                    TestClientDemo client = new TestClientDemo();
                    client.startConnection("127.0.0.1", 8888);
                    String message = messages[count%4];

                    long startTime = System.currentTimeMillis();
                    System.out.println("Client " + count + " sending out: "+ message);
                    String response = client.sendMessage(message);
                    System.out.println("Client " + count + " received: "+ response + " (Elapsed Time: " + (System.currentTimeMillis()-startTime) + "ms)" );

                    //client.stopConnection();
                }
                catch(Exception e){
                    e.printStackTrace();
                }

            }).start();
        }
    }
}
