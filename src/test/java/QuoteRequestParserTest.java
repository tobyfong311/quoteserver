import com.example.model.QuoteRequest;
import com.example.util.QuoteRequestParser;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 */
public class QuoteRequestParserTest {
    @Test
    public void testQuoteServer()  {
        String input;
        QuoteRequest quoteRequest;

        input = "123 BUY 100";
        quoteRequest = QuoteRequestParser.parseInput(input);
        assertEquals(123, quoteRequest.getSecurityId());
        assertEquals(true, quoteRequest.isBuy());
        assertEquals(100, quoteRequest.getQuantity());

        input = "254 SELL 1568";
        quoteRequest = QuoteRequestParser.parseInput(input);
        assertEquals(254, quoteRequest.getSecurityId());
        assertEquals(false, quoteRequest.isBuy());
        assertEquals(1568, quoteRequest.getQuantity());

        // --------- Below are error cases -----------

        input = "254 SELL 1568.4";
        quoteRequest = QuoteRequestParser.parseInput(input);
        assertNull(quoteRequest);

        input = "254  SELL  1568";
        quoteRequest = QuoteRequestParser.parseInput(input);
        assertNull(quoteRequest);

        input = "254 BUG 1568";
        quoteRequest = QuoteRequestParser.parseInput(input);
        assertNull(quoteRequest);
    }
}
