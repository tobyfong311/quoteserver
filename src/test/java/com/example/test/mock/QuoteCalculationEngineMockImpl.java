package com.example.test.mock;

import com.example.marketmaker.QuoteCalculationEngine;

public class QuoteCalculationEngineMockImpl implements QuoteCalculationEngine {

    /**
     *  Return the reference Price Directly after a 3s delay because it is expected to take long time
     * @param securityId     security identifier
     * @param referencePrice reference price to calculate theory price from (e.g. underlying security's price)
     * @param buy            {@code true} if buy, otherwise sell
     * @param quantity       quantity for quote
     * @return Always the referencePrice
     */
    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  referencePrice;
    }
}
