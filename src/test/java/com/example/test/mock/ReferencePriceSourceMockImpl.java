package com.example.test.mock;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;


/**
 *  This mock class is for testing purpose
 *  The initial reference price is always securityId *  2
 *  The price for each security will increase by 1 every 5 seconds
 */
public class ReferencePriceSourceMockImpl implements ReferencePriceSource {
    List<Integer> securityIds;
    List<ReferencePriceSourceListener> listeners = new ArrayList<>();
    ConcurrentHashMap<Integer, Double> refPriceMap = new ConcurrentHashMap();

    public ReferencePriceSourceMockImpl(List<Integer> securityIds){
        this.securityIds = securityIds;

        // Init Ref Price Map
        for (int id:securityIds){
            refPriceMap.put(id, id*2.0);
        }

        // The price for each security will increase by 1 every 5 seconds
        // Just for demo only, the code is designed for a much faster price change rate
        new Timer().scheduleAtFixedRate(new TimerTask() {
            public void run() {
                for (int id:securityIds){
                    double newPrice = refPriceMap.get(id) + 1;
                    refPriceMap.put(id, newPrice);

                    // Notify all the subscribed listener
                    for (ReferencePriceSourceListener listener:listeners){
                        listener.referencePriceChanged(id, newPrice);
                    }
                }
            }
        }, 5000, 5000);
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        listeners.add(listener);
    }

    @Override
    public double get(int securityId) {
        return refPriceMap.get(securityId);
    }
}
